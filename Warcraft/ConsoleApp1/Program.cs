﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monstres;

namespace Monstres
{
    public class Peasant
    {
        public string name;
        public string type;
        public string race;
        public string faction;
        public int hit_points;
        public int armor;

        public Peasant(string name, string type, string race, string faction, int hit_points, int armor)
        {
            this.name = name;
            this.type = type;
            this.race = race;
            this.faction = faction;
            this.hit_points = hit_points;
            this.armor = armor;
        }
        public string sayHello()
        {
            return String.Format("Je suis un {0} , de la race des {1}. Je fais partie des {2}. j'ai {3} points de vie et {4} d'armure", this.type, this.race, this.faction, this.hit_points, this.armor);
        }
        public string talk()
        {
            return "T qui toi et tu veux quoi";
        }
        public string talkToPeasant(Peasant peasant)
        {
            return String.Format("Tu veux quoi {0}", peasant.name);
        }
    }

    public class Peon
    {
        public string name;
        public string type;
        public string race;
        public string faction;
        public int hit_points;
        public int armor;

        public Peon(string name, string type, string race, string faction, int hit_points, int armor)
        {
            this.name = name;
            this.type = type;
            this.race = race;
            this.faction = faction;
            this.hit_points = hit_points;
            this.armor = armor;
        }
        public string sayHello()
        {
            return String.Format("Je suis un {0} , de la race des {1}. Je fais partie de l'{2}. j'ai {3} points de vie et {4} d'armure", this.type, this.race, this.faction, this.hit_points, this.armor);
        }
        public string grunt()
        {
            return "ROOOOOAAAARRRR";
        }

        public string talkToPeon(Peon peon)
        {
            return String.Format("J'vais t'attaqué est tu vas mourrir {0}", peon.name);
        }
    }
}

