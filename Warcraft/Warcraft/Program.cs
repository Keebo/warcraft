﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monstres;


namespace WarcraftConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Peasant Humain_Gilbert, Humain_Patrick;
            Peon Orc_Pim, Orc_Pam;
            Humain_Gilbert = new Peasant("Gilbert", "Peasant", "Human", "Alliance", 30, 0);
            Humain_Patrick = new Peasant("Patrick", "Peasant", "Human", "Alliance", 30, 0);
            Console.WriteLine(Humain_Gilbert.sayHello());
            Console.WriteLine(Humain_Patrick.talk());
            Console.WriteLine(Humain_Gilbert.talkToPeasant(Humain_Patrick));
            Orc_Pim = new Peon("Pim", "Peon", "Orc", "Horde", 30, 0);
            Orc_Pam = new Peon("Pam", "Peon", "Orc", "Horde", 30, 0);
            Console.WriteLine(Orc_Gilbert.sayHello());
            Console.WriteLine(Orc_Pam.grunt());
            Console.WriteLine(Orc_Pam.talkToPeon(Orc_Gilbert));
            Console.ReadKey();

        }
    }
}